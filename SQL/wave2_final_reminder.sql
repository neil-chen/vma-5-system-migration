SELECT 
    NEWID() AS id,
    getUTCdate() AT TIME ZONE 'UTC' AT TIME ZONE 'Aus Eastern Standard Time' AS createdate,
    'C0800318' AS mcid,
    A.original_cellular_no,
    A.sfmc_service_number AS service_number,
    A.firstname AS customer_name,
    Convert(varchar(30),getUTCdate() AT TIME ZONE 'UTC' AT TIME ZONE 'Aus Eastern Standard Time',112) + '020000' AS sms_date,
    A.saville_account_number,
    CASE
        WHEN B.sms_permission = 'True' AND B.promotion_permission <> 'No' THEN
            ('REMINDER: We recently got in touch to let you know that the Virgin Mobile brand is being phased out in Australia and that we would like to switch you to an Optus plan. We''ve sent the account holder an email with more details. This service will be switched between ' 
            + DATENAME(day,[migration_from_date]) + ' ' + DATENAME(month,[migration_from_date]) + ' ' + DATENAME(year,[migration_from_date]) +
            ' to '
            + DATENAME(day,[migration_to_date]) + ' ' + DATENAME(month,[migration_to_date]) + ' ' + DATENAME(year,[migration_to_date]) +
            '. If you do not want to be switched, you have until '
            + DATENAME(day,[internal_opt_out_expiry_date]) + ' ' + DATENAME(month,[internal_opt_out_expiry_date]) + ' ' + DATENAME(year,[internal_opt_out_expiry_date]) +
            ' to opt out by visiting virg.in/Opt-Out. If you do opt out, your existing Virgin Mobile service will be cancelled on 30 June 2020. Visit virg.in/faqs_ or contact us 1300 555 100 for more information. Unsub at virg.in/unsub'
            )
        ELSE
            ('REMINDER: We recently got in touch to let you know that the Virgin Mobile brand is being phased out in Australia and that we would like to switch you to an Optus plan. We''ve sent the account holder an email with more details. This service will be switched between ' 
            + DATENAME(day,[migration_from_date]) + ' ' + DATENAME(month,[migration_from_date]) + ' ' + DATENAME(year,[migration_from_date]) +
            ' to '
            + DATENAME(day,[migration_to_date]) + ' ' + DATENAME(month,[migration_to_date]) + ' ' + DATENAME(year,[migration_to_date]) +
            '. If you do not want to be switched, you have until '
            + DATENAME(day,[internal_opt_out_expiry_date]) + ' ' + DATENAME(month,[internal_opt_out_expiry_date]) + ' ' + DATENAME(year,[internal_opt_out_expiry_date]) +
            ' to opt out by visiting virg.in/Opt-Out. If you do opt out, your existing Virgin Mobile service will be cancelled on 30 June 2020. Visit virg.in/faqs_ or contact us 1300 555 100 for more information.'
            )
    END AS sms_text
FROM
    [BATCHMIGRATION2_NOTIFICATION_ENTRYDE] A
    INNER JOIN [DT_DLY_SERVICE_SEGMENTATION] B ON A.original_cellular_no = B.original_cellular_no
WHERE
    A.migration_batch_no = 'VMA-DT-2'
    AND A.dt_opt_out_status = 'No'
    AND A.dt_edm1_send_status = 'Successful'
    AND (A.internal_edm_campaignid_flag = 'C0800301' OR A.internal_edm_campaignid_flag = 'C0800307')