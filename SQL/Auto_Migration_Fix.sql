SELECT 
    C.original_cellular_no
FROM
    [_Sent] A
    INNER JOIN [_Job] B ON A.JobID = B.JobID AND B.EmailName = 'C0800301_Initial_Notification_Email_Promo_Yes_Batch1'
    INNER JOIN [BATCHMIGRATION1_NOTIFICATION_ENTRYDE_NO_COMMS] C ON A.SubscriberKey = C.original_cellular_no
    LEFT JOIN [_Open] D ON D.SubscriberKey = C.original_cellular_no AND A.JobID = D.JobID AND D.IsUnique = 'True' AND (D.EventDate >= CONVERT(datetime,'04-21-2020'))
    LEFT JOIN [_Click] E ON E.SubscriberKey = C.original_cellular_no AND A.JobID = E.JobID AND E.IsUnique = 'True' AND (E.EventDate >= CONVERT(datetime,'04-21-2020'))
    LEFT JOIN [_Bounce] F ON F.SubscriberKey = C.original_cellular_no AND A.JobID = F.JobID AND F.IsUnique = 'True' AND (F.EventDate >= CONVERT(datetime,'04-21-2020'))
WHERE
    F.SubscriberKey IS NOT NULL
    AND D.SubscriberKey IS NOT NULL

UNION ALL

SELECT 
    C.original_cellular_no
FROM
    [_Sent] A
    INNER JOIN [_Job] B ON A.JobID = B.JobID AND (B.EmailName = 'C0800301_Initial_Notification_Email_Promo_Yes_Batch2' OR B.EmailName = 'C0800307_Initial_Notification_Email_Promo_No_Batch2')
    INNER JOIN [BATCHMIGRATION2_NOTIFICATION_ENTRYDE_NO_COMMS] C ON A.SubscriberKey = C.original_cellular_no
    LEFT JOIN [_Open] D ON D.SubscriberKey = C.original_cellular_no AND A.JobID = D.JobID AND D.IsUnique = 'True' AND (D.EventDate >= CONVERT(datetime,'05-03-2020'))
    LEFT JOIN [_Click] E ON E.SubscriberKey = C.original_cellular_no AND A.JobID = E.JobID AND E.IsUnique = 'True' AND (E.EventDate >= CONVERT(datetime,'05-03-2020'))
    LEFT JOIN [_Bounce] F ON F.SubscriberKey = C.original_cellular_no AND A.JobID = F.JobID AND F.IsUnique = 'True' AND (F.EventDate >= CONVERT(datetime,'05-03-2020'))
WHERE
    F.SubscriberKey IS NULL

SELECT 
    A.original_cellular_no
FROM
    [BATCHMIGRATION1_NOTIFICATION_ENTRYDE_NO_COMMS] A
    LEFT JOIN [_Sent] B ON B.SubscriberKey = A.original_cellular_no
    LEFT JOIN [_Job] C ON B.JobID = C.JobID AND C.EmailName = 'C0800301_Initial_Notification_Email_Promo_Yes_Batch1'
    LEFT JOIN [_Open] D ON D.SubscriberKey = A.original_cellular_no AND B.JobID = D.JobID AND D.IsUnique = 'True' AND (D.EventDate >= CONVERT(datetime,'04-21-2020'))
    LEFT JOIN [_Click] E ON E.SubscriberKey = A.original_cellular_no AND B.JobID = E.JobID AND E.IsUnique = 'True' AND (E.EventDate >= CONVERT(datetime,'04-21-2020'))
    LEFT JOIN [_Bounce] F ON F.SubscriberKey = A.original_cellular_no AND B.JobID = F.JobID AND F.IsUnique = 'True' AND (F.EventDate >= CONVERT(datetime,'04-21-2020'))
WHERE
    B.SubscriberKey IS NULL
