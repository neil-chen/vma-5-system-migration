SELECT 
    NEWID() AS id,
    getUTCdate() AT TIME ZONE 'UTC' AT TIME ZONE 'Aus Eastern Standard Time' AS createdate,
    'C0800302' AS mcid,
    A.original_cellular_no,
    A.sfmc_service_number AS service_number,
    A.firstname AS customer_name,
    Convert(varchar(30),getUTCdate() AT TIME ZONE 'UTC' AT TIME ZONE 'Aus Eastern Standard Time',112) + '020000' AS sms_date,
    A.saville_account_number,
    CASE
        WHEN B.sms_permission = 'True' AND B.promotion_permission <> 'No' THEN
            ('Hi there. As a reminder, the Virgin Mobile brand is being phased out in Australia by June 2020. We''d like to switch you to an Optus plan to keep you connected, which is now more important than ever. This service will be switched between ' 
            + DATENAME(day,[migration_from_date]) + ' ' + DATENAME(month,[migration_from_date]) + ' ' + DATENAME(year,[migration_from_date]) +
            ' to '
            + DATENAME(day,[migration_to_date]) + ' ' + DATENAME(month,[migration_to_date]) + ' ' + DATENAME(year,[migration_to_date]) +
            '. We''ve sent the account holder an email with more details. You can opt out of the switch by visiting virg.in/Opt-Out by '
            + DATENAME(day,[internal_opt_out_expiry_date]) + ' ' + DATENAME(month,[internal_opt_out_expiry_date]) + ' ' + DATENAME(year,[internal_opt_out_expiry_date]) +
            '. If you do opt out, your existing Virgin Mobile service will be cancelled on 30 June 2020. Visit virg.in/faqs_ or contact us 1300 555 100. Unsub at virg.in/unsub'
            )
        ELSE
            ('Hi there.  we need to let you know the Virgin Mobile brand is being phased out in Australia by June 2020.  We’d like to switch you to an Optus plan to keep you connected. This service will be switched between ' 
            + DATENAME(day,[migration_from_date]) + ' ' + DATENAME(month,[migration_from_date]) + ' ' + DATENAME(year,[migration_from_date]) +
            ' to '
            + DATENAME(day,[migration_to_date]) + ' ' + DATENAME(month,[migration_to_date]) + ' ' + DATENAME(year,[migration_to_date]) +
            '. We''ve sent the account holder an email with more details. You can opt out of the switch by visiting virg.in/Opt-Out by '
            + DATENAME(day,[internal_opt_out_expiry_date]) + ' ' + DATENAME(month,[internal_opt_out_expiry_date]) + ' ' + DATENAME(year,[internal_opt_out_expiry_date]) +
            '. If you do opt out, your existing Virgin Mobile service will be cancelled on 30 June 2020. Visit virg.in/faqs_ or contact us 1300 555 100.'
            )
    END AS sms_text
FROM
    [BATCHMIGRATION2_NOTIFICATION_ENTRYDE] A
    INNER JOIN [DT_DLY_SERVICE_SEGMENTATION] B ON A.original_cellular_no = B.original_cellular_no
    LEFT JOIN [BATCHMIGRATION2_NOTIFICATION_SMS] C ON A.original_cellular_no = C.original_cellular_no
WHERE
    A.migration_batch_no = 'VMA-DT-2'
    AND A.dt_opt_out_status = 'No'
    AND A.dt_edm1_send_status = 'Successful'
    AND (A.internal_edm_campaignid_flag = 'C0800301' OR A.internal_edm_campaignid_flag = 'C0800307')
    AND ISNULL(C.original_cellular_no,'') = ''